
// Include Joi package to validate request params and payload.
var Joi = require('joi');
var funciones = require('./funciones');


module.exports = [{
    method: 'GET',      // Methods Type
    path: '/api/user',  // Url
    config: {
        // Include this API in swagger documentation
        tags: ['api'],
        description: 'Descripcion: Obtengo todos los datos de usuarios',
        notes: 'Notas: Obtengo todos los datos de usuarios'
    },
    handler: funciones.getAll
},{
    method: 'POST',
    path: '/api/user',
    config: {
        // "tags" enable swagger to document API
        tags: ['api'],
        description: 'Save user data',
        notes: 'Save user data',
        // We use Joi plugin to validate request
        validate: {
            payload: {
                // Both name and age are required fields
                name: Joi.string().required(),
                age: Joi.number().required()
            }
        }
    },
    handler: funciones.newUser
},{
    method: 'GET',
    //Getting data for particular user "/api/user/1212313123"
    path: '/api/user/{id}',
    config: {
        tags: ['api'],
        description: 'Get specific user data',
        notes: 'Get specific user data',
        validate: {
            // Id is required field
            params: {
                id: Joi.string().required()
            }
        }
    },
    handler: funciones.getOne
},{
    method: 'PUT',
    path: '/api/user/{id}',
    config: {
        // Swagger documentation fields tags, description, note
        tags: ['api'],
        description: 'Update specific user data',
        notes: 'Update specific user data',

        // Joi api validation
        validate: {
            params: {
                //`id` is required field and can only accept string data
                id: Joi.string().required()
            },
            payload: {
                name: Joi.string(),
                age: Joi.number()
            }
        }
    },
    handler: funciones.updateUser
},{
    method: 'DELETE',
    path: '/api/user/{id}',
    config: {
        tags: ['api'],
        description: 'Remove specific user data',
        notes: 'Remove specific user data',
        validate: {
            params: {
                id: Joi.string().required()
            }
        }
    },
    handler: funciones.deleteUser
}]
