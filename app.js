// ================ Base Setup ========================
// Include Hapi package
var Hapi = require('hapi');

var routes = require('./rutas');


// Create Server Object
var server = new Hapi.Server();

// Include Mongoose ORM to connect with database
var mongoose = require('mongoose');

// Making connection with `restdemo` database in your local machine
mongoose.connect('mongodb://localhost/restdemo');


// Define PORT number
server.connection({port: 7002});

// Register Swagger Plugin ( Use for documentation and testing purpose )
server.register([{
    register: require('hapi-swagger'),
    options: {
        apiVersion: "0.0.1"
    }
},{
    register: require('good'),
    options: {
        opsInterval: 1000,
        reporters: [{
            reporter: require('good-console'),
            events: {log: '*', response: '*'}
        }]
    }
}] , function (err) {
    if (err) {
        server.log(['error'], 'hapi-swagger load error: ' + err);
        console.error(err);
    } else {
        server.log(['start'], 'hapi-swagger interface loaded');
    }
});




// =============== Routes for our API =======================
// Define GET route
 routes.forEach( ( route ) => {

            console.log( 'attaching:' + route.path + " with " + route.method );
            server.route( route );
        } );


// =============== Start our Server =======================
// Lets start the server
server.start(function () {
    console.log('Server running at:', server.info.uri);
});

